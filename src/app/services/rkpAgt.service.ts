import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class RkpAgtService {
    constructor(private http : Http){}

    //include token at header
    token = localStorage.getItem("token");
    headers = new Headers({ 'x-access-token': this.token });
    options = new RequestOptions({ headers: this.headers });

    url : string = "http://api.wifi-s.com";

    getAll(userId){
        return this.http.get(this.url+'/v1/agt/rekap/'+userId, this.options)
            .map(res => res.json())
    }
    getByMonth(userId, m){
        return this.http.get(this.url+'/v1/agt/rekap/'+userId+"/"+m, this.options)
            .map(res => res.json())
    }    
    create(data){
        return this.http.post(this.url+'/v1/agt/rekap', data, this.options)
            .map(res => res.json())
    }
    
    
}