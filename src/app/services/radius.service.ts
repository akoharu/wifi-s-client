import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class RadiusService {
    constructor(private http : Http){}

    //include token at header
    token = localStorage.getItem("token");
    headers = new Headers({ 'x-access-token': this.token });
    options = new RequestOptions({ headers: this.headers });

    url : string = "http://api.wifi-s.com";

    getVoucher(){
        return this.http.get(this.url+'/v1/radius/voucher', this.options)
            .map(res => res.json())
    }
    getVoucherDetail(voucherId){
        return this.http.get(this.url+'/v1/radius/voucher/detail/'+voucherId, this.options)
            .map(res => res.json())
    }    
     createVoucher(data){
        return this.http.post(this.url+'/v1/radius/generate/voucher', data ,this.options)
            .map(res => res.json())
    }

    
    
}