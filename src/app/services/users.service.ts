import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {
    constructor(private http : Http){}

    //include token at header
    token = localStorage.getItem("token");
    headers = new Headers({ 'x-access-token': this.token });
    options = new RequestOptions({ headers: this.headers });

    url : string = "http://api.wifi-s.com";

    getAll(){
        return this.http.get(this.url+'/v1/profile', this.options)
            .map(res => res.json())
    }
    getId(id){
        return this.http.get(this.url+'/v1/profile/'+id, this.options)
            .map(res => res.json())
    }
    create(newUser){
        return this.http.post(this.url+'/v1/profile/register/', newUser, this.options)
            .map(res => res.json())
    }
    update(id ,user){
        return this.http.put(this.url+'/v1/profile/edit/'+id, user, this.options)
            .map(res => res.json())
    }
    destroy(id){
        return this.http.delete(this.url+'/v1/profile/delete/'+id, this.options)
            .map(res => res.json())
    }
    getBalance(id){
        return this.http.get(this.url+'/v1/profile/saldo/'+id, this.options)
            .map(res => res.json())
    }
    getPersentase(id){
        return this.http.get(this.url+'/v1/profile/persentase/'+id, this.options)
            .map(res => res.json())
    }
    
    getNoHP(id){
        return this.http.get(this.url+'/v1/profile/phone/'+id, this.options)
            .map(res => res.json())
    }

    updateBalance(id, balance){
        return this.http.put(this.url+'/v1/profile/saldo/'+id, balance, this.options)
            .map(res => res.json())
    }    

    balanceMinus(id, nominal){
        return this.http.get(this.url+'/v1/saldo/minus/'+id+'/'+nominal, this.options)
            .map(res => res.json())
    }
    updateFeeMkt(id, nominal){
        return this.http.put(this.url+'/v1//profile/feemkt/'+id, nominal, this.options)
            .map(res => res.json())
    }
    getAllAgents(idDst){
        return this.http.get(this.url+'/v1/dst/'+idDst+'/agent/all', this.options)
            .map(res => res.json())
    }
    createAgents(idDst, data){
        return this.http.post(this.url+'/v1/dst/agent/'+idDst, data, this.options)
            .map(res => res.json())
    }
    updateAgents(idDst, idAgt, data){
        return this.http.put(this.url+'/v1/dst/'+idDst+'/agent/'+idAgt, data, this.options)
            .map(res => res.json())
    }           
    addSaldoAgent(idDst, idAgt, nominal){
        return this.http.get(this.url+'/v1/dst/'+idDst+'/agent/'+idAgt+'/add/'+nominal, this.options)
            .map(res => res.json())
    }    
}