import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AgentGuardService {

    constructor(private auth: AuthService, private router: Router) {}
    canActivate() {
        if (this.auth.profile().Role.profile === "agt") {
            return true;
        }
            this.router.navigate(['login']);
            return false;
    }    
}