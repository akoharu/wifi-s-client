import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class RkpMktService {
    constructor(private http : Http){}

    //include token at header
    token = localStorage.getItem("token");
    headers = new Headers({ 'x-access-token': this.token });
    options = new RequestOptions({ headers: this.headers });

    url : string = "http://api.wifi-s.com";

    getAll(){
        return this.http.get(this.url+'/v1/mkt/rekap', this.options)
            .map(res => res.json())
    }
    getAllByM(m){
        return this.http.get(this.url+'/v1/mkt/rekap/m/'+m, this.options)
            .map(res => res.json())
    }
    create(data){
        return this.http.post(this.url+'/v1/mkt/rekap', data, this.options)
            .map(res => res.json())
    }
    
    
}