import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AdminGuardService {

    constructor(private auth: AuthService, private router: Router) {}
    canActivate() {
        if (this.auth.profile().Role.profile === "adm") {
            return true;
        }
            this.router.navigate(['login']);
            return false;
    }    
}