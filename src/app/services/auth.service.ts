import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { tokenNotExpired, JwtHelper } from "angular2-jwt";
import 'rxjs/add/operator/map';
//js component
import "./fungsiAing.js";
declare var fungsiAing : any ;
@Injectable()
export class AuthService {
    url : string = "http://api.wifi-s.com";
    jwtHelper: JwtHelper = new JwtHelper();
    constructor(private http : Http, private router : Router){}
    login(credentials){
        this.http.post(this.url+'/authenticate', credentials)
            .map(res => res.json())
            .subscribe(
                data => {
                    if(data.success){
                        localStorage.setItem('token', data.token);
                        this.redirect();
                    }else{
                        fungsiAing.showNotification("danger", "error", "ID atau password salah");
                    }
                },
                error => console.log(error)
            )
    }

    profile(){
        var _token = localStorage.getItem('token');
        return this.jwtHelper.decodeToken(_token);
    }

    loggedIn() {
        return tokenNotExpired();
    }

    redirect(){
        if(this.loggedIn()){
            let profil = this.profile();
            if(profil.Role.profile === "adm"){
                this.router.navigateByUrl("/admin")
            }else if(profil.Role.profile === "dst"){
                this.router.navigateByUrl("distri")
            }else{
                this.router.navigateByUrl("agent")
        }
        }        
    }

    logout(){
        localStorage.removeItem('token');
        this.router.navigateByUrl("login")
    }
}