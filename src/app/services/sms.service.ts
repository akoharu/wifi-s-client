import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class SmsService {
    constructor(private http : Http){}

    //include token at header
    // token = localStorage.getItem("token");
    // headers = new Headers({ 'x-access-token': this.token });
    // options = new RequestOptions({ headers: this.headers });

    url : string = "http://sms.agti.co.id";

    sendSms(noHp, pesan){
        return this.http.get(this.url+'/playsms/index.php?app=ws&u=wifis001&h=8a0a11c3ef5064bac185b3e1ccc1d681&op=pv&to='+noHp+'&msg='+pesan)
            .map(res => res.json())
    }
    
}