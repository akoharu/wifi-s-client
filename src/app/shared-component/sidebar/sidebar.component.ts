import { Component, OnInit, Input } from '@angular/core';
import { sidebarMenu } from './sidebar-menu';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers:[AuthService]
})
export class SidebarComponent implements OnInit {
  userProfile = <any>{};
  @Input() menus : sidebarMenu[];
  constructor(private auth : AuthService) { }

  ngOnInit() {
    this.userProfile = this.auth.profile()
  }

}
