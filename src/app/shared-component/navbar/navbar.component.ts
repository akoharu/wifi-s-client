import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service"
//js component
import "../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers : [AuthService]
})
export class NavbarComponent implements OnInit {

  title = "WIFI-S Billing System";

  constructor(private authService : AuthService) { }

  openSidebar(){
     fungsiAing.openSidebar();   
  }
  logout(){
    var r = confirm("Apakah anda yakin ?");
    if (r == true){
      this.authService.logout();
    }
  }  
  ngOnInit() {
    fungsiAing.initComponent();
    fungsiAing.openSidebar();
  }

}
