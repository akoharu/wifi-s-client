export class Users {
    id: number;
    idUser: string;
    password: string;
    name: string;
    area: string;
    email: string;
    noHp: string;
    role: string;
    persentase: 0;
    balance : {
        value : number
    }
}