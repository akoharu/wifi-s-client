import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { sidebarMenu } from '../shared-component/sidebar/sidebar-menu';
import { AgentMenu } from "app/agent/agent-menu";

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.css']
})
export class AgentComponent implements OnInit {

  agentMenu : sidebarMenu [];
  constructor(private router : Router) { }

  ngOnInit() {
    this.agentMenu = AgentMenu
    this.router.navigateByUrl("agent/dashboard")
  }

}
