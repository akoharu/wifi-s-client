import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AgentComponent } from './agent.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RekapitulasiComponent } from './rekapitulasi/rekapitulasi.component';

const AgentRoutes : Routes = [{
    path : '',
    component : AgentComponent,
    children : [
        {
            path : 'dashboard',
            component : DashboardComponent
        },
        {
            path : 'rekap',
            component : RekapitulasiComponent
        }
    ]
}]
@NgModule({
    declarations: [],
    imports: [ CommonModule, RouterModule.forChild(AgentRoutes) ],
    exports: [ RouterModule],
    providers: [],
    bootstrap: []
})
export class AgentRoutingModule {}