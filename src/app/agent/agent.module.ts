import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { SharedComponentModule } from '../shared-component/shared-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgentRoutingModule } from "app/agent/agent-routing.module";

import { AgentComponent } from './agent.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RekapitulasiComponent } from './rekapitulasi/rekapitulasi.component';
import { ModalTransaksiComponent } from './dashboard/modal-transaksi/modal-transaksi.component';

@NgModule({
  imports: [
    CommonModule,
    AgentRoutingModule,
    SharedComponentModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [
    AgentComponent,
    DashboardComponent,
    RekapitulasiComponent,
    ModalTransaksiComponent
  ]
})
export class AgentModule { }
