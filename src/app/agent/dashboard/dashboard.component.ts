import { Component, OnInit } from '@angular/core';
import { UsersService } from "app/services/users.service";
import { AuthService } from "app/services/auth.service";
import { RkpAgtService } from "app/services/rkpAgt.service";
import { Subject } from "rxjs/Rx";
//js component
import '../../services/fungsiAing.js';
declare var fungsiAing : any ;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers : [UsersService, AuthService, RkpAgtService]
})

export class DashboardComponent implements OnInit {
  dtOptions: any;
  dtTrigger: Subject <any> = new Subject();
  rekapDsts = []  
  idKu : number;
  saldoKu : number;
  rekapKu = [];
  
  date = new Date();
  month = this.date.getMonth()+1;    
  onSelectMonth(m){
    this.rkpAgt.getByMonth(this.idKu, m)
      .subscribe(
        data => {
          this.rekapKu = data;
          // Calling the DT trigger to manually render the table
          this.dtTrigger.next();  
        }
      )
  }
  constructor(
    private usersService : UsersService, 
    private auth : AuthService,
    private rkpAgt : RkpAgtService
  ) { }

  ngOnInit() {
    this.idKu = this.auth.profile().id
    this.usersService.getBalance(this.idKu)
      .subscribe(
        data => {
          this.saldoKu = data.nominal
        }
      )
    fungsiAing.initForm();
    fungsiAing.initModal();
    this.onSelectMonth(this.month);    
    this.dtOptions = {
      retrieve: true,
      dom : 'Bfrtip',
      buttons : ['csv', 'copy', 'excel']
    }
  }

}
