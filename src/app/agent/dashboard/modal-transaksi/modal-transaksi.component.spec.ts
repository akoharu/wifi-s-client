import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTransaksiComponent } from './modal-transaksi.component';

describe('ModalTransaksiComponent', () => {
  let component: ModalTransaksiComponent;
  let fixture: ComponentFixture<ModalTransaksiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTransaksiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTransaksiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
