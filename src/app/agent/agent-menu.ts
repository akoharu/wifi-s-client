import {sidebarMenu} from '../shared-component/sidebar/sidebar-menu';

export const AgentMenu : sidebarMenu[] = [
    {
      name: 'Dashboard',
      icon: 'dashboard',
      url: 'dashboard'
    },
    {
      name: 'Rekapitulasi',
      icon: 'book',
      url: 'rekap'
    }

]