import { Component, OnInit } from '@angular/core';
import { UsersService } from "app/services/users.service";
import { AuthService } from "app/services/auth.service";
import { RkpAgtService } from "app/services/rkpAgt.service";

//js component
import '../../services/fungsiAing.js';
declare var fungsiAing : any ;

@Component({
  selector: 'app-rekapitulasi',
  templateUrl: './rekapitulasi.component.html',
  styleUrls: ['./rekapitulasi.component.css'],
  providers : [UsersService, AuthService, RkpAgtService]  
})

export class RekapitulasiComponent implements OnInit {
  idKu : number;
  saldoKu : number;
  rekapKu = [];

  constructor(
    private usersService : UsersService, 
    private auth : AuthService,
    private rkpAgt : RkpAgtService
  ) { }

  onSelectMonth(m){
    this.rkpAgt.getByMonth(this.idKu, m.bulan)
      .subscribe(
        data => {
          this.rekapKu = data
          console.log(data)
        }
      )
  }
  ngOnInit() {
    fungsiAing.initForm();
    this.idKu = this.auth.profile().id
  }

}
