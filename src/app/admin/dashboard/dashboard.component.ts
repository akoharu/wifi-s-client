import { Subject } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { RkpAdmService } from '../../services/rkpAdm.service';

//js component
import '../../services/fungsiAing.js';
declare var fungsiAing : any ;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers : [RkpAdmService]
})
export class DashboardComponent implements OnInit {
  dtOptions : any = {};
  dtTrigger : Subject<any> = new Subject;
  rekapDsts: any;
  totalSaldoBersih: number;
  totalSaldoBank: number;
  //saldo BUlanan
  sJan : number;
  sFeb : number;
  sMar : number;
  sApr : number;
  sMei : number;
  sJun : number;
  sJul : number;
  sAgt : number;
  sSep : number;
  sOkt : number;
  sNov : number;
  sDes : number;

  date = new Date();
  month = this.date.getMonth()+1;
  constructor(private rkpAdmService : RkpAdmService) { }
  onSelectMonth(mm){
    this.rkpAdmService.getAllByM(mm)
      .subscribe(
        data => {
          this.totalSaldoBank = 0;
          this.totalSaldoBersih = 0;
          this.rekapDsts = data;
          this.dtTrigger.next();
          data.forEach(element => {
            this.totalSaldoBank += parseInt(element.saldoBank);
            this.totalSaldoBersih += parseInt(element.saldoBersih);
          });
        },
        err => console.log(err)
      )
  }
  saldoBulanan(mm){
    this.rkpAdmService.getSaldoByM(mm)
      .subscribe(
        data => {
          if(mm == 1){
            this.sJan = data
          }else if(mm == 2){
            this.sFeb = data
          }else if(mm == 3){
            this.sMar = data
          }else if(mm == 4){
            this.sApr = data
          }else if(mm == 5){
            this.sMei = data
          }else if(mm == 6){
            this.sJun = data
          }else if(mm == 7){
            this.sJul = data
          }else if(mm == 8){
            this.sAgt = data
          }else if(mm == 9){
            this.sSep = data
          }else if(mm == 10){
            this.sOkt = data
          }else if(mm == 11){
            this.sNov = data
          }else if(mm == 12){
            this.sDes = data
          }else if(mm == 13){
             fungsiAing.initDataSale(
               this.sJan, this.sFeb, this.sMar, this.sApr,
               this.sMei, this.sJun, this.sJul, this.sAgt, 
               this.sSep, this.sOkt, this.sNov, this.sDes
            );
          }
        }
      )
  }
  refresh(){
    this.ngOnInit()
  }    
  ngOnInit() {
    fungsiAing.initForm();
    this.onSelectMonth(this.month);
    for(let i = 1; i<=13; i++){
      this.saldoBulanan(i);
    }
    this.dtOptions = {
      retrieve: true,      
      dom : 'Bfrtip',
      buttons : ['csv', 'copy', 'excel']
    }
  }

}
