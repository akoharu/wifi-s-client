import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { sidebarMenu } from '../shared-component/sidebar/sidebar-menu';
import { AdminMenu } from './admin-menu';
//js component
import "../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  adminMenu : sidebarMenu [];

  constructor(private router : Router) { }

  ngOnInit() {
    this.adminMenu = AdminMenu;
    this.router.navigateByUrl("admin/dashboard")
  }

}
