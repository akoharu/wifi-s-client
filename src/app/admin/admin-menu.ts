import {sidebarMenu} from '../shared-component/sidebar/sidebar-menu';

export const AdminMenu : sidebarMenu[] = [
    {
      name: 'Dashboard',
      icon: 'dashboard',
      url: 'dashboard'
    }, {
      name: 'Users',
      icon: 'account_circle',
      url: 'users'
    }, {
      name: 'Rekapitulasi',
      icon: 'book',
      url: 'rekap'
    }, {
      name: 'Fee MKT',
      icon: 'supervisor_account',
      url: 'feemkt'
    }

]