import { Pipe, Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

//js component
import "../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers : [UsersService]
})
export class UserComponent implements OnInit {
  SelectedUser = {};
  dtOptions: DataTables.Settings = {};
  users = [];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject <any> = new Subject();

  constructor(private usersService : UsersService, private router : Router) { }
  onSelectUser(user){
    this.SelectedUser = user;
    console.log(user);
  }
  deleteUser(user){
    var r = confirm("Apakah anda yakin, ingin menghapus "+user.name+" ?");
    if(r == true){
      this.usersService.destroy(user.id).subscribe(
        data => {
          console.log(data)
          if(data == true){
            fungsiAing.showSwal("success", user.name+" berhasil dihapus!")                  
            this.getUsers();
          }else{
            fungsiAing.showSwal("failed", user.name+" gagal dihapus!")                  
          }
        }
      )
    }
  }

  getUsers(){
    this.usersService.getAll()
          .subscribe(
              data => {
                this.users = data;
                // Calling the DT trigger to manually render the table
                this.dtTrigger.next();  
              },
              error => {console.log(error);}
          )    
  }  
  ngOnInit() {
    this.getUsers();
    fungsiAing.initForm();
    fungsiAing.initModal();
    this.dtOptions = {
      retrieve: true      
    }
  }

}
