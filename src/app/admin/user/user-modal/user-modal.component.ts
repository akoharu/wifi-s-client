import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {UserComponent} from "../user.component"

import {UsersService} from "../../../services/users.service";
import {RkpAdmService} from "../../../services/rkpAdm.service";
import {RkpDstService} from "../../../services/rkpDst.service";
import {RkpMktService} from "../../../services/rkpMkt.service";
import {SmsService} from "../../../services/sms.service";

//js component
import "../../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css'],
  providers : [UsersService, RkpAdmService, RkpDstService, RkpMktService, SmsService]
})

export class UserModalComponent implements OnInit {

  @Input() selectedUser = <any>{}
  constructor(
    private usersService : UsersService,
    private rkpAdmService : RkpAdmService,
    private rkpDstService : RkpDstService,
    private rkpMktService : RkpMktService,
    private userComponent : UserComponent, 
    private http : Http,
    private router : Router,
    private sms : SmsService
    ) { }

  ngOnInit() {
  }

  randomPass(){
    var x = Math.floor((Math.random() * 10000000) + 1);
    return x;
  }
  addUser(newUser){
    newUser.password = this.randomPass();
    newUser.idUser = newUser.profile+this.randomPass();
    this.usersService.create(newUser)
      .subscribe(
        data => {
          if(data.success){
            var pesan = "Selamat+bergabung+di+WIFI-S,+User+ID+=+"+newUser.idUser+"+password+=+"+newUser.password+",+silahkan+login+di+my.wifi-s.com";
            this.http.get("http://sms.agti.co.id/playsms/index.php?app=ws&u=wifis001&h=8a0a11c3ef5064bac185b3e1ccc1d681&op=pv&to="+newUser.noHp+"&msg="+pesan)
              .map(
                res => res.json()
              )
              .subscribe(
                data => {
                  fungsiAing.showSwal("success", newUser.name+" berhasil ditambahkan !")                  
                  this.userComponent.ngOnInit();
                }
              )
          }else{
                  fungsiAing.showSwal("failed", newUser.name+" Gagal ditambahkan !")                  
          }
        }
      )
  }

  editUser(id, dataUser){
    this.usersService.update(id, dataUser)
      .subscribe(
        data => {
          if(data == 1){
            fungsiAing.showSwal("success", "Data berhasil diubah !")                  
            this.userComponent.ngOnInit()
          }else{
            fungsiAing.showSwal("failed", "Data gagal diubah !")                  
          }
        }
      )
  }

  addBalance(id, balance){
    console.log(balance)
    this.usersService.getId(id)
      .subscribe(
        data => {
          let saldoAwal = data.Balance.nominal
          let tambahSaldo = balance.saldo
          let persentase = data.persentase
          let tambahSaldoPlus = tambahSaldo + (tambahSaldo*(persentase/100))
          let feeMKt = data.FeeMkt.nominal

          let saldoBaru = saldoAwal+tambahSaldoPlus
          let jatahMkt = tambahSaldo*(feeMKt/100)
          let saldoBersih = tambahSaldo - jatahMkt
          let saldoBank = tambahSaldo

          balance.nominal = saldoBaru
          this.usersService.updateBalance(id, balance)
            .subscribe(
              data => {
                if(data == 1){
                  //masuk rekap admin
                  this.rkpAdmService.create({
                    userId : id,
                    saldoBersih : saldoBersih,
                    saldoBank : saldoBank
                  })
                    .subscribe(
                      data => {
                        //masuk rekap marketing
                        this.rkpMktService.create({
                          userId : id,
                          nominal : jatahMkt
                        })
                          .subscribe(
                            data => {
                              //masuk rekap distri
                              this.rkpDstService.create({
                                userId : id,
                                aksi : "deposit-dst",
                                setor : tambahSaldo,
                                saldo : tambahSaldoPlus,
                                sisaSaldo : saldoBaru,                               
                              })
                                .subscribe(
                                  data => {
                                    if(data.id){
                                      fungsiAing.showSwal("success", "Saldo RP."+tambahSaldoPlus+" berhasil ditambahkan !")                  
                                      // send sms to DST
                                      let noHP : number
                                      this.usersService.getNoHP(id)
                                        .subscribe(
                                          data => {
                                            noHP = data.noHp
                                            var pesan = "Saldo+anda+berhasil+ditambahkan+sebesar+Rp.+"+tambahSaldoPlus+".+Terimakasih+"
                                            this.sms.sendSms(noHP, pesan)
                                              .subscribe(
                                                data => {
                                                  this.userComponent.ngOnInit()
                                                }
                                              )
                                          }
                                        )
                                    }
                                  }
                                )
                            }
                          )
                      }
                    )
                }else{
                  fungsiAing.showSwal("failed", "Saldo gagal ditambahkan !")                  
                }
              } 
            )
          
        }
      )  
  }

  editFeeMKT(id, nominal){
    this.usersService.updateFeeMkt(id, nominal)
      .subscribe(
        data => {
          if(data == 1){
            fungsiAing.showSwal("success", "Fee berhasil diubah !")                  
            this.userComponent.ngOnInit()
          }else{
            fungsiAing.showSwal("failed", "Fee gagal diubah !")                  
          }          
        }
      )
  }

}
