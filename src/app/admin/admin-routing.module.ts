import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component'
import { RekapitulasiComponent } from './rekapitulasi/rekapitulasi.component'
import { FeeMarketingComponent } from './fee-marketing/fee-marketing.component'
import { HistoryComponent } from './history/history.component'

const adminRoutes : Routes = [
    {
        path : '',
        component : AdminComponent,
        children : [
            {
                path : 'dashboard',
                component : DashboardComponent
            },
            {
                path : 'users',
                component : UserComponent
            },
            {
                path : 'rekap',
                component : RekapitulasiComponent
            },
            {
                path : 'feemkt',
                component : FeeMarketingComponent
            },
            {
                path : 'history',
                component : HistoryComponent
            }
        ]
    }
]
@NgModule({
    declarations: [],
    imports: [ 
        CommonModule,
        RouterModule.forChild(adminRoutes)
    ],
    exports: [ RouterModule ],
    providers: [],
    bootstrap: []
})
export class AdminRoutingModule {}