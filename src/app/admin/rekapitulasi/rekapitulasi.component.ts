import { Data } from 'webdriver-js-extender/built/spec/mockdriver';
import { Component, OnInit } from '@angular/core';
import {RkpAdmService} from "../../services/rkpAdm.service";
//js component
import "../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-rekapitulasi',
  templateUrl: './rekapitulasi.component.html',
  styleUrls: ['./rekapitulasi.component.css'],
  providers : [RkpAdmService]
})
export class RekapitulasiComponent implements OnInit {
  rekapDsts = [];
  constructor(
    private rkpAdmService: RkpAdmService
  ) { }

  totalSaldoBersih = 0;
  totalSaldoBank = 0;
  onSelectMonth(mm){
    this.rkpAdmService.getAllByM(mm.bulan)
      .subscribe(
        data => {
          this.totalSaldoBank = 0;
          this.totalSaldoBersih = 0;
          this.rekapDsts = data;

          data.forEach(element => {
            this.totalSaldoBank += parseInt(element.saldoBank);
            this.totalSaldoBersih += parseInt(element.saldoBersih);
          });
        },
        err => console.log(err)
      )
  }
  ngOnInit() {
    fungsiAing.initForm();
  }


}
