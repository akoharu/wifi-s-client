import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {UserFilter} from "../user-filter.pipe"
import { DataTablesModule } from 'angular-datatables';

import { SharedComponentModule } from '../shared-component/shared-component.module'

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminRoutingModule } from './admin-routing.module';
import { UserComponent } from './user/user.component';
import { UserModalComponent } from './user/user-modal/user-modal.component';
import { RekapitulasiComponent } from './rekapitulasi/rekapitulasi.component';
import { FeeMarketingComponent } from './fee-marketing/fee-marketing.component';
import { HistoryComponent } from './history/history.component'


@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedComponentModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [
    AdminComponent,
    DashboardComponent,
    UserComponent,
    UserModalComponent,
    UserFilter,
    RekapitulasiComponent,
    FeeMarketingComponent,
    HistoryComponent
  ]
})
export class AdminModule {}
