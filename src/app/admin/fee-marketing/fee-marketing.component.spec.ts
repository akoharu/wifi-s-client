import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeMarketingComponent } from './fee-marketing.component';

describe('FeeMarketingComponent', () => {
  let component: FeeMarketingComponent;
  let fixture: ComponentFixture<FeeMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
