import { RkpMktService } from '../../services/rkpMkt.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
//js component
import "../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-fee-marketing',
  templateUrl: './fee-marketing.component.html',
  styleUrls: ['./fee-marketing.component.css'],
  providers : [RkpMktService]
})
export class FeeMarketingComponent implements OnInit {
  mktRekaps = [];
  constructor(
    private rkpMktService : RkpMktService
  ) { }

  //Total Fee
  totalFee = 0;

  onSelectMonth(m){
    this.rkpMktService.getAllByM(m.bulan)
      .subscribe(
        data => {
          this.totalFee = 0;
          this.mktRekaps = data;
          data.forEach(element => {
            this.totalFee += parseInt(element.nominal)
          });
        },
        err => console.log(err)
      )    
  }
  ngOnInit() {  
    fungsiAing.initForm();  
  }

}
