import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {Router} from "@angular/router";
//js component
import "../services/fungsiAing.js";
declare var fungsiAing : any ;

interface Credentials {
  idUser : string,
  password : string,
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers : [AuthService]
})
export class LoginComponent implements OnInit {

  credentials : Credentials;

  constructor(private auth : AuthService, private router : Router) { }

  onLogin(credentials){
    this.auth.login(credentials);
  }

  ngOnInit() {
    fungsiAing.initComponent();
    this.auth.redirect();
  }

}
