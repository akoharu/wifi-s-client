import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import {DistributorComponent} from "./distributor.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgentsComponent } from './agents/agents.component';
import { RekapitulasiComponent } from './rekapitulasi/rekapitulasi.component';
import { HistoryComponent } from './history/history.component';

const distriRoutes : Routes = [
    {
        path : '',
        component : DistributorComponent,
        children : [
            {
                path : 'dashboard',
                component : DashboardComponent
            },
            {
                path : 'agents',
                component : AgentsComponent
            },
            {
                path : 'rekap',
                component : RekapitulasiComponent
            },
            {
                path : 'history',
                component : HistoryComponent
            },              
        ]
    }
]
@NgModule({
    declarations: [],
    imports: [ 
        CommonModule,
        RouterModule.forChild(distriRoutes)
    ],
    exports: [ RouterModule ],
    providers: [],
    bootstrap: []
})
export class DistriRoutingModule { }
