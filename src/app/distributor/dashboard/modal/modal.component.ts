import { DashboardComponent } from '../dashboard.component';
import { RkpDstService } from '../../../services/rkpDst.service';
import { RadiusService } from '../../../services/radius.service';
import { UsersService  } from '../../../services/users.service';
import { AuthService } from '../../../services/auth.service';
import { SmsService} from '../../../services/sms.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import '../../../services/fungsiAing.js';
declare var fungsiAing : any ;

@Component({
  selector: 'app-transaksi',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  providers : [RadiusService, UsersService, SmsService, RkpDstService]
})
export class ModalComponent implements OnInit {
  listVouchers = [];
  idUser : number;
  constructor(
    private radiusService : RadiusService,
    private dashboard : DashboardComponent,
    private usersService : UsersService,
    private auth : AuthService,
    private smsService : SmsService,
    private rkpDstService : RkpDstService
  ) { }
  addTransaksi(data){
    let _noHp = data.noHp
    //cek harga voucher
    if(data.idVoucher && data.noHp){
    this.radiusService.getVoucherDetail(data.idVoucher)
      .subscribe(
        data => {
          let hargaVoucher : number = data.price;
          let namaVoucher : string = data.name;
          let saldo : number = this.dashboard.saldoKu;
          //cek saldo
          if(saldo > hargaVoucher){
            //generate voucher
            this.radiusService.createVoucher(data.DataVoucher)
              .subscribe(
                _data => {
                  if(_data.success == true){
                    let _dataVoucher = _data.data[0];
                    console.log(_dataVoucher)                    
                    let wifisID = _dataVoucher.name;
                    let wifisPass = _dataVoucher.password;
                    console.log(wifisID+"###"+wifisPass)                    
                    let smsMsg = "Pembelian+Voucher+WIFI-S+"+namaVoucher+"+berhasil.+ID=+"+wifisID+"+Password="+wifisPass+"+Link+login:+http://hotspot.wifi-s.com/login?username="+wifisID+"%26password="+wifisPass
                    this.smsService.sendSms(_noHp, smsMsg)
                      .subscribe(
                        __data => {
                          if(__data.data[0].status === "OK"){
                           // saldo minus VoucherPrice
                           let saldoAkhir : number = saldo - hargaVoucher;
                           this.usersService.balanceMinus(this.idUser, hargaVoucher)
                            .subscribe(
                              ___data =>{
                                if(___data == 1)
                                // post to rekapitulasi
                                this.rkpDstService.create({
                                  userId : this.idUser,
                                  aksi : "sell-"+namaVoucher,
                                  nominal : hargaVoucher,
                                  sisaSaldo : saldoAkhir,                               
                                })
                                  .subscribe(
                                    ____data => {
                                        if(____data.id){
                                          fungsiAing.showSwal("success", "Vocher berhasil dikirimkan!")
                                          this.dashboard.ngOnInit()
                                        }
                                    }
                                  )
                              }
                            )
                          }

                        }
                      )
                  }
                }
              )

          }else{
            fungsiAing.showSwal("failed", "Saldo anda tidak mencukupi!")            
          }
        }
      )      
    }else{
      fungsiAing.showSwal("failed", "Field harus diisi semua!")
    }
    //cek saldo, apakah kurang dari hargaVoucher ?
  }
  ngOnInit() {
    fungsiAing.initModal()
    this.idUser = this.auth.profile().id
    this.radiusService.getVoucher()
      .subscribe(
        data => {
          this.listVouchers = data.items
          console.log(data)
        }
      )
  }

}
