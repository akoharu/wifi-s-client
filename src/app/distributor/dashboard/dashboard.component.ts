import { Subject } from 'rxjs/Rx';
import { RkpDstService } from '../../services/rkpDst.service';
import { AuthService } from '../../services/auth.service';
import { UsersService } from '../../services/users.service';
import { RadiusService } from '../../services/radius.service';
import { Component, OnInit } from '@angular/core';
//js component
import '../../services/fungsiAing.js';
declare var fungsiAing : any ;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers : [RadiusService, UsersService, AuthService, RkpDstService]
})
export class DashboardComponent implements OnInit {
  dtOptions: any;
  dtTrigger: Subject <any> = new Subject();
  rekapDsts = []  
  userId: number;
  saldoKu : number;
  
  constructor(
    private radiusService : RadiusService,
    private userService : UsersService,
    private authService : AuthService,
    private rkpDstService : RkpDstService
    ) { }

    
  date = new Date();
  month = this.date.getMonth()+1;    
  onSelectMonth(m){
    this.rkpDstService.getByMonth(this.userId, m)
      .subscribe(
        data => {
          this.rekapDsts = data;
          // Calling the DT trigger to manually render the table
          this.dtTrigger.next();  
        }
      )
  }
  ngOnInit() {
    this.userId = this.authService.profile().id
    this.userService.getBalance(this.userId)
      .subscribe(
        data => {
          this.saldoKu = data.nominal
        }
      )
    fungsiAing.initForm();
    fungsiAing.initModal();
    this.onSelectMonth(this.month);    
    this.dtOptions = {
      retrieve: true,
      dom : 'Bfrtip',
      buttons : ['csv', 'copy', 'excel']
    }    
  }

}
