import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {sidebarMenu} from '../shared-component/sidebar/sidebar-menu';
import {DistriMenu} from "./distri-menu";
@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.css']
})
export class DistributorComponent implements OnInit {

  _distriMenu : sidebarMenu [];

  constructor(private router : Router) { }
  
  ngOnInit() {
    this._distriMenu = DistriMenu;
    this.router.navigateByUrl("distri/dashboard")
  }

}
