import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { SharedComponentModule } from '../shared-component/shared-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DistriRoutingModule } from './distri-routing.module';

import {DistributorComponent} from "./distributor.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgentsComponent } from './agents/agents.component';
import { RekapitulasiComponent } from './rekapitulasi/rekapitulasi.component';
import { HistoryComponent } from './history/history.component';
import { ModalComponent } from './dashboard/modal/modal.component';
import { AgentModalComponent } from './agents/agent-modal/agent-modal.component';

@NgModule({
  imports: [
    CommonModule,
    DistriRoutingModule,
    SharedComponentModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [
    DashboardComponent,
    DistributorComponent,
    AgentsComponent,
    RekapitulasiComponent,
    HistoryComponent,
    ModalComponent,
    AgentModalComponent,
  ]
})
export class DistributorModule { }
