import {sidebarMenu} from '../shared-component/sidebar/sidebar-menu';

export const DistriMenu : sidebarMenu[] = [
    {
      name: 'Dashboard',
      icon: 'dashboard',
      url: 'dashboard'
    }, {
      name: 'Agents',
      icon: 'account_circle',
      url: 'agents'
    }, {
      name: 'Rekapitulasi',
      icon: 'book',
      url: 'rekap'
    }

]