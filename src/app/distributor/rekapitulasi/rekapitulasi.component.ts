import { AuthService } from '../../services/auth.service';
import { RkpDstService } from '../../services/rkpDst.service';
import { Component, OnInit } from '@angular/core';
//js component
import "../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-rekapitulasi',
  templateUrl: './rekapitulasi.component.html',
  styleUrls: ['./rekapitulasi.component.css'],
  providers : [RkpDstService, AuthService]
})
export class RekapitulasiComponent implements OnInit {
  userId: any;
  rekapDsts = []

  constructor(
    private rkpDstService : RkpDstService,
    private authService : AuthService
  ) { }

  onSelectMonth(m){
    this.rkpDstService.getByMonth(this.userId, m.bulan)
      .subscribe(
        data => {
          this.rekapDsts = data
          console.log(data)
        }
      )
  }
  ngOnInit() {
    fungsiAing.initForm();
    this.userId = this.authService.profile().id
  }

}
