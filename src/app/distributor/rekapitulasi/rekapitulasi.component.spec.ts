import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RekapitulasiComponent } from './rekapitulasi.component';

describe('RekapitulasiComponent', () => {
  let component: RekapitulasiComponent;
  let fixture: ComponentFixture<RekapitulasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapitulasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RekapitulasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
