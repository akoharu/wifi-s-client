import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { AuthService} from '../../services/auth.service';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
//js component
import "../../services/fungsiAing.js";
declare var fungsiAing : any ;

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.css'],
  providers : [UsersService, AuthService]  
})
export class AgentsComponent implements OnInit {
  idUser : number;
  SelectedUser = {};
  dtOptions: DataTables.Settings = {};
  agents = [];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject <any> = new Subject();
  constructor(private usersService : UsersService, private auth : AuthService) { }
  onSelectUser(agent){
    this.SelectedUser = agent;
    console.log(agent);
  }
  getUsers(){
    this.usersService.getAllAgents(this.idUser)
          .subscribe(
              data => {
                this.agents = data;
                // Calling the DT trigger to manually render the table
                this.dtTrigger.next();  
              },
              error => {console.log(error);}
          )    
  }     
  ngOnInit() {
    this.idUser = this.auth.profile().id
    this.getUsers();
    fungsiAing.initForm();
    fungsiAing.initModal();
    this.dtOptions = {
      retrieve: true      
    }    
  }

}
