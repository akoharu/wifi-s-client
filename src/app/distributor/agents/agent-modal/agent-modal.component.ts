import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {AgentsComponent} from "../agents.component"
import {UsersService} from "../../../services/users.service";
import {RkpDstService} from "../../../services/rkpDst.service";
import {RkpAgtService} from "../../../services/rkpAgt.service";
import { SmsService} from '../../../services/sms.service';
import { AuthService } from '../../../services/auth.service';

import '../../../services/fungsiAing.js';
declare var fungsiAing : any ;

@Component({
  selector: 'app-agent-modal',
  templateUrl: './agent-modal.component.html',
  styleUrls: ['./agent-modal.component.css'],
  providers : [UsersService, RkpDstService, SmsService, RkpAgtService]  
})

export class AgentModalComponent implements OnInit {
  @Input() selectedUser = <any>{};
  idUser : number;
  saldoKu : number;
  constructor(
    private usersService : UsersService,
    private rkpDstService : RkpDstService,
    private agentsComponent : AgentsComponent,
    private sms : SmsService,
    private auth : AuthService,
    private rkpAgtService : RkpAgtService 
    ) { }

  randomPass(){
    var x = Math.floor((Math.random() * 10000000) + 1);
    return x;
  }
  addUser(newUser){
    newUser.password = this.randomPass();
    newUser.idUser = "agt"+this.randomPass();
    let noHp = newUser.noHp;
    this.usersService.createAgents(this.idUser, newUser)
      .subscribe(
        data => {
          if(data.success){
            var pesan = "Selamat+bergabung+di+WIFI-S,+User+ID+=+"+newUser.idUser+"+password+=+"+newUser.password+",+silahkan+login+di+my.wifi-s.com";
            this.sms.sendSms(noHp, pesan)
              .subscribe(
                data => {
                  fungsiAing.showSwal("success", "Berhasil Menambahkan Agent!")
                  this.agentsComponent.ngOnInit();
                }
              )
          }else{
            fungsiAing.showSwal("failed", "Gagal Menambahkan Agent!")
          }
        }
      )
  }

  editUser(id, dataUser){
    this.usersService.updateAgents(this.idUser, id, dataUser)
      .subscribe(
        data => {
          if(data == 1){
            fungsiAing.showSwal("success", "DATA Berhasil diubah!")
            this.agentsComponent.ngOnInit()
          }else{
            fungsiAing.showSwal("failed", "Data user GAGAL di ubah!")
          }
        }
      )
  }

  addBalance(idAgent, balance){
    // cek saldo
    let setorSaldo = parseInt(balance.saldo)
    if(this.saldoKu > setorSaldo){
    //tambah saldo agent + persentase
    let persentase : number;
    this.usersService.getPersentase(idAgent)
      .subscribe(
        _data => {
          persentase = _data.persentase
          // get saldo saldo Agent sebelumnya
          this.usersService.getBalance(idAgent)
            .subscribe(
              __data => {
                let saldoAwalAgent = __data.nominal;
                let tambahanSaldo = setorSaldo + (setorSaldo*(persentase/100))
                // input Saldo Agent
                this.usersService.addSaldoAgent(this.idUser, idAgent, tambahanSaldo)
                  .subscribe(
                    ___data => {
                      // kurangi saldo distri
                      this.usersService.balanceMinus(this.idUser, tambahanSaldo)
                        .subscribe(
                          data => {
                            // masuk rekap
                            this.rkpDstService.create({
                                userId : this.idUser,
                                aksi : "deposit-agt",
                                setor : setorSaldo,
                                saldo : tambahanSaldo,
                                sisaSaldo : this.saldoKu - tambahanSaldo,                               
                            })
                              .subscribe(
                                ____data => {
                                      fungsiAing.showSwal("success", "Saldo"+tambahanSaldo+"Berhasil ditambahkan!")  
                                      //sendSms to agent
                                      this.usersService.getNoHP(idAgent)
                                        .subscribe(
                                          data => {
                                            let noHp = data.noHp
                                            var pesan = "Saldo+anda+berhasil+ditambahkan+sebesar+Rp."+tambahanSaldo+".+Terimakasih!+"
                                            this.sms.sendSms(noHp, pesan)
                                              .subscribe(
                                                data => {
                                                  alert("Pesan Berhasil dikirim!")
                                                  // masuk rekap agt
                                                  this.rkpAgtService.create({
                                                    userId : idAgent,
                                                    aksi : "deposit",
                                                    nominal : tambahanSaldo,
                                                    sisaSaldo : saldoAwalAgent+tambahanSaldo
                                                  })
                                                    .subscribe(
                                                      hasil => {
                                                        this.agentsComponent.ngOnInit()
                                                      }
                                                    )
                                                }
                                              )
                                          }
                                        )
                                }
                              )
                          }
                        )
                    }
                  )                                
              }
            )            
        }
      )    
    }else{
      fungsiAing.showSwal("failed", "Saldo anda tidak mencukupi!")      
    }

  }
  ngOnInit() {
    this.idUser = this.auth.profile().id
    this.usersService.getBalance(this.idUser)
      .subscribe(
        data => {
          this.saldoKu = parseInt(data.nominal)
        }
      )
  }
}
