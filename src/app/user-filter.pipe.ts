import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'userFilter'
})
export class UserFilter implements PipeTransform {
    transform(items: Array<any>, q: string): Array<any> {
        if(q === undefined || q === "") return items;
        return items.filter(item => item.idUser === q);
    }
}