import { NgModule } from '@angular/core';
import { AuthGuard } from './services/auth-guard.service';
import { AdminGuardService } from './services/admin-guard.service';
import { DistriGuardService } from './services/distri-guard.service';

import { AuthService } from './services/auth.service';

import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { NotfoundComponent } from './notfound/notfound.component';
import { LoginComponent } from './login/login.component';


const appRoutes : Routes = [
    {
        path : 'admin',
        loadChildren: 'app/admin/admin.module#AdminModule',
        canActivate : [AuthGuard, AdminGuardService]
    },
    {
        path : 'distri',
        loadChildren: 'app/distributor/distributor.module#DistributorModule',
        canActivate : [AuthGuard, DistriGuardService]
    },
    {
        path : 'agent',
        loadChildren: 'app/agent/agent.module#AgentModule',
        canActivate : [AuthGuard]
    },    
    {
        path : 'login',
        component : LoginComponent
    },
    {
        path : '',
        component : LoginComponent
    },
    {
        path : '**',
        component : NotfoundComponent
    }
]

@NgModule({
    declarations: [],
    imports: [ 
        CommonModule,
        RouterModule.forRoot(appRoutes)         
    ],
    exports: [ 
        RouterModule
    ],
    providers: [AuthGuard, AuthService, AdminGuardService, DistriGuardService],
    bootstrap: []
})
export class AppRoutingModule {}