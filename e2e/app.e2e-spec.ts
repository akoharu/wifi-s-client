import { WifiSPage } from './app.po';

describe('wifi-s App', () => {
  let page: WifiSPage;

  beforeEach(() => {
    page = new WifiSPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
